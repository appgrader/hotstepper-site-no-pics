{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{if !isset($content_only) || !$content_only}
					</div><!-- #center_column -->
					{if isset($right_column_size) && !empty($right_column_size)}
						<div id="right_column" class="col-xs-12 col-sm-{$right_column_size|intval} column">{$HOOK_RIGHT_COLUMN}</div>
					{/if}
					</div><!-- .row -->
				</div><!-- #columns -->
			</div><!-- .columns-container -->
			{if isset($HOOK_FOOTER)}
				<!-- Footer -->
				<div class="footer-container">
					<footer id="footer"  class="container">
						<div class="row">
						<div class="col-md-8 col-left">
							{*{$HOOK_FOOTER}*}

			<div class="colfooter col-md-4">
				<div class="footer_h2">Suport clienti</div>
				<ul>
					<li><a href="/content/5-cum-cumpar">Cum cumpar?</a></li>
					<li><a href="/content/6-cum-platesc">Cum platesc?</a></li>
					<li><a href="/content/1-livrare">Livrarea produselor</a></li>
					<li><a href="/content/7-garantii">Garantia produselor</a></li>
					<li><a href="/content/21-returnarea-produselor">Returnarea produselor</a></li>
				</ul>
			</div>
			<div class="colfooter col-md-4">
				<div class="footer_h2">Oferta noastra</div>
				<ul>
					<li><a href="/13-lichidari-de-stoc">Lichidari de stoc</a></li>
					<li><a href="/produse-noi">Produse noi</a></li>
					<li><a href="/content/11-ghid-de-marimi">Ghid de marimi</a></li>
					<li><a href="/content/12-pantofi-la-comanda">Pantofi la comanda</a></li>
				</ul>

			</div>
			<div class="colfooter col-md-4">
				<div class="footer_h2">Informatii</div>
				<ul>
					<li><a href="/contact">Contact</a></li>
<!-- 					<li>Despre noi</li> -->
					<li><a href="/content/3-termeni-si-conditii">Termeni si conditii</a></li>
					<li><a href="/content/10-politica-de-confidentialitate">Confidentialitate</a></li>
					<li><a href="http://www.anpc.gov.ro/" target="_blank">ANPC</a></li>
				</ul>
			</div>

						</div>
						<div class="newsletter col-md-4">
							<form action="">
							<input type="text" class="nl-input" placeholder="E-MAIL" value="">
							</form>
						</div>
						</div>
					</footer>
					<div class="container">
					© Copyright 2015 Hotstepper SRL
					<br><br>

				</div><!-- #footer -->
			{/if}
		</div><!-- #page -->
{/if}
{include file="$tpl_dir./global.tpl"}
</body>
</html>

