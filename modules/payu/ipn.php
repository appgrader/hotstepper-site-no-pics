<?php


global $smarty;
require_once(dirname(__FILE__).'/../../config/config.inc.php');
include(dirname(__FILE__).'/../../init.php');
require_once("LiveUpdate.class.php");	


class PayuIpnModuleFrontController extends Module
{

	public $ssl = true;
	public $secret_key;
	public $is_idn_active;
	public $merchant_id;

	public function postProcess()
	{


		if(isset($_POST)): 
			/* Make sure strlen behaves as intended by setting multibyte function overload to 0*/
		ini_set("mbstring.func_overload", 0);
		if(ini_get("mbstring.func_overload") > 2){  /* check if mbstring.func_overload is still set to overload strings(2)*/
		echo "WARNING: mbstring.func_overload is set to overload strings and might cause problems\n";
		}

/* Internet Payment Notification */
$this->getPayuKeys();

$pass		= trim($this->secret_key);	/* pass to compute HASH */
$result		= ""; 				/* string for compute HASH for received data */
$return		= ""; 				/* string to compute HASH for return result */
$signature	= $_POST["HASH"];	/* HASH received */
$body		= "";

/* read info received */
ob_start();
while(list($key, $val) = each($_POST)){
	$$key=$val;

	/* get values */
	if($key != "HASH"){

		if(is_array($val)) $result .= $this->ArrayExpand($val);
		else{
			$size		= strlen(StripSlashes($val));
			$result	.= $size.StripSlashes($val);
		}
	}
}
$body = ob_get_contents();
ob_end_flush();

$date_return = date("YmdGis");

$return = strlen($_POST["IPN_PID"][0]).$_POST["IPN_PID"][0].strlen($_POST["IPN_PNAME"][0]).$_POST["IPN_PNAME"][0];
$return .= strlen($_POST["IPN_DATE"]).$_POST["IPN_DATE"].strlen($date_return).$date_return;

$hash =  $this->hmac($pass, $result); /* HASH for data received */
$body .= $result."\r\n\r\nHash: ".$hash."\r\n\r\nSignature: ".$signature."\r\n\r\nReturnSTR: ".$return;

if($hash == $signature){
	
	echo "Verified OK!";

    /* ePayment response */

    $result_hash =  $this->hmac($pass, $return);
	echo "<EPAYMENT>".$date_return."|".$result_hash."</EPAYMENT>";

    $payment_accepted_code = 2;

    if($_POST['ORDERSTATUS'] == 'PAYMENT_AUTHORIZED' || $_POST['ORDERSTATUS'] == 'PAYMENT_RECEIVED'|| $_POST['ORDERSTATUS'] == 'TEST')
    {






	$history = new OrderHistory();
	$history->id_order = (int)($_POST['REFNOEXT']);
	$history->changeIdOrderState(2, $history->id_order);
	$history->addWithemail();
	$history->save();


    $order_ref_no = $_POST['REFNO'];
    $order_currency = $_POST['CURRENCY'];
    $order_amount = $_POST['IPN_TOTALGENERAL'];

	$date_return = date("YmdGis");

    $return2  = strlen($this->merchant_id).$this->merchant_id.strlen($order_ref_no).$order_ref_no;
    $return2 .= strlen($order_amount ).$order_amount.strlen($order_currency).$order_currency;    
	$return2 .= strlen($date_return).$date_return;


    $current_hash = $this->hmac($pass,$return2);

    if($this->is_idn_active == "1")
    $this->curlPost($order_ref_no,$order_amount,$order_currency,$date_return ,$current_hash);


    }


}
endif;
	}

	/**
	 * @see FrontController::initContent()
	 */
	public function initContent()
	{
		$this->display_column_left = false;
		$this->display_column_center = false;
		$this->display_footer = false;
		$this->display_header = false;
		parent::initContent();

		$this->context->smarty->assign(array(
			'total' => $this->context->cart->getOrderTotal(true, Cart::BOTH),
			'this_path' => $this->module->getPathUri(),
			'this_path_ssl' => Tools::getShopDomainSsl(true, true).__PS_BASE_URI__.'modules/'.$this->module->name.'/'
		));

		$this->setTemplate('ipn.tpl');
	}


	public function ArrayExpand($array){
	$retval = "";
	for($i = 0; $i < sizeof($array); $i++){
		$size		= strlen(StripSlashes($array[$i]));
		$retval	.= $size.StripSlashes($array[$i]);
	}

	return $retval;
}

public function hmac ($key, $data){
   $b = 64; // byte length for md5
   if (strlen($key) > $b) {
       $key = pack("H*",md5($key));
   }
   $key  = str_pad($key, $b, chr(0x00));
   $ipad = str_pad('', $b, chr(0x36));
   $opad = str_pad('', $b, chr(0x5c));
   $k_ipad = $key ^ $ipad ;
   $k_opad = $key ^ $opad;
   return md5($k_opad  . pack("H*",md5($k_ipad . $data)));
}



public function getPayuKeys(){


		/* GETS THE SECRET KEY OF PAYU */
		$this->merchant_id = Configuration::get('payu_merchant_id'); 

		/* GETS THE SECRET KEY OF PAYU */
		$this->secret_key = Configuration::get('payu_secret_key'); 


		/* GETS THE PAYU STATE - IS TEST */
		$this->is_test = Configuration::get('payu_state_is_test'); 

		/* GETS THE PAYU STATE - IS IDN ACTIVE */
		$this->is_idn_active = Configuration::get('payu_state_is_idn_active'); 



	}


public function curlPost($order_ref,$order_amount,$order_currency,$idn_date,$order_hash){

$url = 'https://secure.epayment.ro/order/idn.php';

$fields = array(
            'MERCHANT' => urlencode($this->merchant_id),
            'ORDER_REF' => urlencode($order_ref),
            'ORDER_AMOUNT' => urlencode($order_amount),
            'ORDER_CURRENCY' => urlencode($order_currency),           
            'IDN_DATE' => urlencode($idn_date),
            'ORDER_HASH' => urlencode($order_hash)
        );


$fields_string ="";
foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
rtrim($fields_string, '&');

$ch = curl_init();
curl_setopt($ch,CURLOPT_URL, $url);
curl_setopt($ch,CURLOPT_POST, count($fields));
curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
$result = curl_exec($ch);
curl_close($ch);

}

}

$ipn = new PayuIpnModuleFrontController();
$ipn->postProcess();