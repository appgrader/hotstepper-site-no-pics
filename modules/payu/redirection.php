<?php
global $smarty;

require_once(dirname(__FILE__).'/../../config/config.inc.php');
include(dirname(__FILE__).'/../../header.php');
include(dirname(__FILE__).'/../../init.php');

require_once("LiveUpdate.class.php");	
require_once("payu.php");	

class PayuRedirectionModuleFrontController extends Module
{
	public $ssl = true;
	public $merchant_id;
	public $secret_key;
	public $order_id;
	public $customer_email;
	public $is_test;
	public $order_total;
	public $live_update_url;
	public $form_html;
	public $redirect_timer;
	public $redirect_to_shop;
	public $redirect_auto_html;
	public $shipping_price;

	public function postProcess()
	{


		global $cookie;

		$payu = new Payu();

		$cart = new Cart((int)($cookie->id_cart));
		$currency_order = new Currency((int)($cart->id_currency));
		
		$this->shipping_price = $cart->getOrderTotal(true, Cart::ONLY_SHIPPING);


		$address = new Address((int)($cart->id_address_delivery));
		$country = new Country((int)($address->id_country));
		$state = NULL;
		if ($address->id_state)
		$state = new State((int)($address->id_state));

		$customer = new Customer((int)($cart->id_customer));

		$cart_id  = (int)($cart->id).'_'.pSQL($cart->secure_key);
		$customer = new Customer((int)$cart->id_customer);
		$total = $cart->getOrderTotal(true, Cart::BOTH);
		$this->order_total = $total;
		$payu->validateOrder((int)$cart->id, Configuration::get('PS_OS_PREPARATION'), $total, $payu->displayName, NULL, array(), NULL, false, $customer->secure_key);






	if (Tools::getValue('confirm'))
			{	

				$total = (float)($cart->getOrderTotal(true, Cart::BOTH));
				$this->order_id = $payu->currentOrder;
			}

				$this->generateForm();


		}




	public function generateForm(){

	$this->getPayuKeys();


	$errOutput		= "";


	$myLiveUpdate 	= new LiveUpdate(trim($this->secret_key));				
	$myLiveUpdate->setMerchant($this->merchant_id);
	$myLiveUpdate->setOrderDate(date("Y-m-d H:i:s"));


	$myLiveUpdate->setOrderRef($this->order_id);

	$order = new Order($this->order_id);


	$id_customer = $order->id_customer;
	$this->getCustomerEmail($id_customer);

	$PName		= array();		
	$PCode		= array();	
	$PPrice		= array();
	$PPriceType	= array();		
	$PQTY		= array();	
	$PVAT		= array();



	foreach($order->getProducts() as $product):



	$PName[]	= $product['product_name'];	
	$PCode[]	= $product['product_id'];
	$PPrice[]	= $product['product_price'];
	$PQTY[]		= $product['product_quantity'];
	$PPriceType[] = 'NET';
	$PVAT[]		= $product['tax_rate'];
	
	endforeach;


	$PName[]	= 'Taxa transport';	
	$PCode[]	= 'TT';
	$PPrice[]	= $this->shipping_price;
	$PQTY[]		= 1;
	$PPriceType[] = 'NET';
	$PVAT[]		= 0;


	$myLiveUpdate->setOrderPName($PName);
	$myLiveUpdate->setOrderPCode($PCode);
	$myLiveUpdate->setOrderPrice($PPrice);
	$myLiveUpdate->setOrderPType($PPriceType);
	$myLiveUpdate->setOrderQTY($PQTY);
	$myLiveUpdate->setOrderVAT($PVAT);
	
	 global $cookie;

	 $currency = $cookie->id_currency; 
	 $currency_obj = Currency::getCurrency($cookie->id_currency);


	$myLiveUpdate->setPricesCurrency($currency_obj['iso_code']);

    $id_address_delivery = $order->id_address_delivery;
    $id_address_invoice = $order->id_address_invoice;


    $the_delivery_address = new Address($id_address_delivery);
    $the_invoice_address = new Address( $id_address_invoice);
	$country_iso_delivery = Country::getIsoById($the_delivery_address->id_country);
	$country_iso_billing = Country::getIsoById($the_invoice_address->id_country);



	$billing = array(
		"billFName"				=> $the_invoice_address->firstname,
		"billLName"				=> $the_invoice_address->lastname,
		"billCompany"			=> $the_invoice_address->company,
		"billPhone" 			=> $the_invoice_address->phone,
		"billAddress1"			=> $the_invoice_address->address1,
		"billAddress2"			=> $the_invoice_address->address2,
		"billZipCode"			=> $the_invoice_address->postcode,
		"billCity"				=> $the_invoice_address->city,
		"billCountryCode"		=> $country_iso_billing,
		"billEmail" 			=> $this->customer_email 
	);

	$myLiveUpdate->setBilling($billing);


	$delivery = array(
		"deliveryFName"				=> $the_delivery_address->firstname,
		"deliveryLName"				=> $the_delivery_address->lastname,
		"deliveryCompany"			=> $the_delivery_address->company,
		"deliveryPhone" 			=> $the_delivery_address->phone,
		"deliveryAddress1"			=> $the_delivery_address->address1,
		"deliveryAddress2"			=> $the_delivery_address->address2,
		"deliveryZipCode"			=> $the_delivery_address->postcode,
		"deliveryCity"				=> $the_delivery_address->city,
		"deliveryCountryCode"		=> $country_iso_delivery
	);

	$myLiveUpdate->setDelivery($delivery);
	$myLiveUpdate->setOrderShipping(-1);

	if($this->is_test == "1")
	$myLiveUpdate->setTestMode(true);




	$this->live_update_url = $myLiveUpdate->liveUpdateURL;
	$this->form_html = $myLiveUpdate->getLiveUpdateHTML();	

	if($this->redirect_to_shop == "1"):
		$domain = Tools::getShopDomain();
		$this->redirect_auto_html  = '<input type="hidden" name="AUTOMODE" VALUE="1" />	<input type="hidden" name="BACK_REF" VALUE="http://'.$domain.'" />';
		endif;
	}


	 public function getPayuKeys(){


		/* GETS THE SECRET KEY OF PAYU */
		$this->merchant_id = Configuration::get('payu_merchant_id'); 

		/* GETS THE SECRET KEY OF PAYU */
		$this->secret_key = Configuration::get('payu_secret_key'); 


		/* GETS THE PAYU STATE - IS TEST */
		$this->is_test = Configuration::get('payu_state_is_test'); 

		/* GETS THE PAYU STATE - IS IDN ACTIVE */
		$this->is_idn_active = Configuration::get('payu_state_is_idn_active'); 

		/* GETS THE PAYU REDIRECT TIMER */
		$this->redirect_timer = Configuration::get('payu_redirect_timer'); 

		/* GETS THE PAYU REDIRECT TO SHOP*/
		$this->redirect_to_shop = Configuration::get('payu_redirect_to_shop'); 
	}

	public function getCustomerEmail($cid){


		$customer = new Customer((int)$cid);
		$this->customer_email = $customer->email;
	}

}
	$payu = new PayuRedirectionModuleFrontController();

	$payu->postProcess();

	$smarty->assign(array( 	'this_path_ssl' => Tools::getShopDomainSsl(true, true).__PS_BASE_URI__.'modules/payu/'	));

	$smarty->assign(array( 	'total' => $total,
							'form_html' =>  $payu->form_html,
							'live_update_url' => $payu->live_update_url,
							'redirect_timer'=> $payu->redirect_timer,
							'redirect_html' => $payu->redirect_auto_html
								));



	$smarty->assign('this_path', __PS_BASE_URI__.'modules/payu/');
	$template = '/redirection.tpl';

	$smarty->display(dirname(__FILE__).$template);


	include(dirname(__FILE__).'/../../footer.php');