{*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14011 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{capture name=path}{l s='Shipping' mod='payu'}{/capture}
<div class="blog_outter_wrapper">
{capture name=path}<a href="{$link->getPageLink('my-account', true)}">{l s='My account'}</a><span class="navigation-pipe">{$navigationPipe}</span>{l s='Order history'}{/capture}
{include file="$tpl_dir./errors.tpl"}
<div class="h2_wrapper_down">
<h2 class="productscategory_h2 sprite">
{l s='Order summation' mod='payu'}
</h2>
<div class="clear"></div>
</div>


{assign var='current_step' value='payment'}
{include file="$tpl_dir./order-steps.tpl"}
<br/><br/>
<h3>{l s='Pay with Epayment - Payu' mod='payu'}</h3>
<br/><br/>
<form action="{$base_dir_ssl}modules/payu/redirection.php" method="post">
	<input type="hidden" name="confirm" value="1" />
	<p>
		<img src="{$this_path}payu_logo.png" alt="{l s='Payu.ro' mod='payu'}" style="float:left; margin: 0px 10px 5px 0px;" />
		<br/><br />
		{l s='The total amount of your order is' mod='payu'}
		<span id="amount_{$currencies.0.id_currency}" class="price">{convertPrice price=$total}</span>
		{if $use_taxes == 1}
		    {l s='(tax incl.)' mod='payu'}
		{/if}
	</p>
		<p class="confirm_order_now" style="clear:both; font-weight:bold;">{l s='Please confirm your order by clicking \'I confirm my order\'' mod='payu'}.</p>

	<p class="cart_navigation">
		<a href="{$link->getPageLink('order', true)}?step=3" class="button_large">{l s='Other payment methods' mod='payu'}</a>
		<input type="submit" name="submit" value="{l s='I confirm my order' mod='payu'}" class="cart_quantity_next fright button_large" />
	</p>
</form>
</div>