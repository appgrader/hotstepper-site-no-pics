<?php


if (!defined('_PS_VERSION_'))
	exit;

class Payu extends PaymentModule
{	

	public $merchant_id;
	public $secret_key;
	public $is_test;
	public $is_idn_active;
	public $redirect_timer;
	public $redirect_to_shop;
	public $error;

	public function __construct()
	{
		$this->name = 'payu';
		$this->tab = 'payments_gateways';
		$this->version = '3';
		$this->author = 'http://www.graphicvision.ro';
		$this->need_instance = 1;
		$this->currencies = true;
		$this->currencies_mode = 'checkbox';
		$this->module_key= "4481a6f088a06b881fd00074cea2e87c";

		parent::__construct();

		$this->displayName = $this->l('Payu.ro');
		$this->description = $this->l('Pay with payu.ro');
		$this->confirmUninstall = $this->l('Are you sure you want to uninstall PAYU PAYMENT MODULE?');

		/* For 1.4.3 and less compatibility */
		
		$updateConfig = array('PS_OS_CHEQUE', 'PS_OS_PAYMENT', 'PS_OS_PREPARATION', 'PS_OS_SHIPPING', 'PS_OS_CANCELED', 'PS_OS_REFUND', 'PS_OS_ERROR', 'PS_OS_OUTOFSTOCK', 'PS_OS_BANKWIRE', 'PS_OS_PAYPAL', 'PS_OS_WS_PAYMENT');
		if (!Configuration::get('PS_OS_PAYMENT'))
			foreach ($updateConfig as $u)
				if (!Configuration::get($u) && defined('_'.$u.'_'))
					Configuration::updateValue($u, constant('_'.$u.'_'));
	}

	public function install()
	{


		if (!parent::install() OR !$this->registerHook('payment') OR  !$this->registerHook('displayHeader') OR !$this->registerHook('paymentReturn'))
			return false;
		return true;
	}


	public function uninstall()
	{
	  parent::uninstall();
	}



	public function hookDisplayHeader()
	{
		
		$this->context->controller->addCSS(($this->_path).'payu.css', 'all');
	}



	public function hookPayment($params)
	{
		if (!$this->active)
			return ;

		global $smarty;

		// Check if cart has product download
		foreach ($params['cart']->getProducts() AS $product)
		{
			$pd = ProductDownload::getIdFromIdProduct((int)($product['id_product']));
			if ($pd AND Validate::isUnsignedInt($pd))
				return false;
		}

		$smarty->assign(array(
			'this_path' => $this->_path,
			'this_path_ssl' => Tools::getShopDomainSsl(true, true).__PS_BASE_URI__.'modules/'.$this->name.'/'
		));
		return $this->display(__FILE__, 'payment.tpl');
	}
	
	public function hookPaymentReturn($params)
	{
		if (!$this->active)
			return ;

		return $this->display(__FILE__, 'confirmation.tpl');
	}



	public function getContent()
	{

		
	
	
	   	$this->_html = '<h2>'.$this->displayName.'</h2>';



			if (Tools::isSubmit('btnSubmit'))
		{
			

			$err = 0;

			if(empty($_POST['merchant_id']) || $_POST['merchant_id'] == '' || empty($_POST['secret_key']) || $_POST['secret_key'] == '')
				$err = 1;

			if($err == 0)
				{


					/* UPDATES THE MERCHANT_ID OF PAYU*/
					Configuration::updateValue('payu_merchant_id', $_POST['merchant_id']); 

					/* UPDATES THE SECRET KEY OF PAYU */
					Configuration::updateValue('payu_secret_key', $_POST['secret_key']); 


					/* UPDATES THE PAYU STATE - IS TEST */
					Configuration::updateValue('payu_state_is_test', $_POST['istest']); 

					/* UPDATES THE PAYU STATE - IS IDN ACTIVE */
					Configuration::updateValue('payu_state_is_idn_active',$_POST['is_idn_active']); 

					/* UPDATES THE PAYU REDIRECT TIMER */
					Configuration::updateValue('payu_redirect_timer',(int)($_POST['redirect_timer'])); 
					
					/* UPDATES THE PAYU REDIRECT TO SHOP */
					Configuration::updateValue('payu_redirect_to_shop',(int)($_POST['merchant_redirect'])); 


				}
				else
				{
				$this->error .= '<span class="error">'. $this->l('Merchant ID & Secret Key cannot be NULL .').'</span><br />';

				}

			}

				$this->getPayuKeys();

		$this->_displayPayU();
		$this->_displayForm();

		return $this->_html;
	}

private function _displayPayU()
	{

		  $this->_html .= '<img src="../modules/payu/payu.jpg" width="" style="float:left; margin-right:15px;"><b>'.$this->l('This module allows you to accept payments with Payu (www.payu.ro).').'</b><br /><br />';
		  $this->_html .= ' <b>'.$this->l('Module created by').' <a target="_blank" href="http://www.graphicvision.ro"><u>GraphicVision</u></a></b>';
		  $this->_html .= ' <br /><br /><br /><br />';

	}

	private function _displayForm()
	{

			$test_mode_options = array(0 => "", 1 => "");
			$idn_active_options = array(0 => "", 1 => "");
			$redirect_shop_options = array(0 => "", 1 =>"");

		/* GETS THE STATUS OF THE IS TEST OPTION */
		if($this->is_test == '1')
			$test_mode_options[0]= "checked";
		else
			$test_mode_options[1]= "checked";

		/* GETS THE STATUS OF THE IS IDN ACTIVE*/
		if($this->is_idn_active == '1')
			$idn_active_options[0] = "checked";
		else
			$idn_active_options[1] = "checked";


		/* GETS THE STATUS OF THE IS IDN ACTIVE*/
		if($this->redirect_to_shop == '1')
			$redirect_shop_options[0] = "checked";
		else
			$redirect_shop_options[1] = "checked";





		$this->_html .= "<style>#payu_admin_form label,#payu_admin_form td { font-size:13px; }</style>";
		$this->_html .=
		'<form action="'.Tools::htmlentitiesUTF8($_SERVER['REQUEST_URI']).'" method="post" id="payu_admin_form">
			<fieldset>
			<legend><img src="../img/admin/contact.gif" />'.$this->l('Payu Merchant details').'</legend>
				<table border="0" width="800" cellpadding="0" cellspacing="0" id="form">
					<tr><td  width: style="height: 35px;">'.$this->l('Merchant ID').'</td><td><input  type="text" style="width:300px;" name="merchant_id" value="'.$this->merchant_id.'"/></td></tr>
					<tr><td  style="height: 35px;">'.$this->l('Secret Key').'</td><td><input  type="text" style="width:300px;" name="secret_key" value="'.$this->secret_key.'"/></td></tr>
					<tr><td  style="height: 35px;">'.$this->l('Test mode?').'</td><td><label for="istest" style="width:inherit">No <input   '.$test_mode_options[1].' type="radio" name="istest" value="0"/></label><label style="width:inherit" for="istest">Yes <input '.$test_mode_options[0].'  type="radio" name="istest" value="1" /></label></td></tr>
					<tr><td  style="height: 35px;">'.$this->l('IDN(Instant Delivery Notification) ?').'</td><td><label for="is_idn_active" style="width:inherit">No <input   '.$idn_active_options[1].' type="radio" name="is_idn_active" value="0"/></label><label style="width:inherit" for="is_idn_active">Yes <input '.$idn_active_options[0].'  type="radio" name="is_idn_active" value="1" /></label></td></tr>
					<tr><td  style="height: 35px;">'.$this->l('Auto redirect consumer ?').'</td><td><label for="merchant_redirect" style="width:inherit">No <input   '.$redirect_shop_options[1].' type="radio" name="merchant_redirect" value="0"/></label><label style="width:inherit" for="merchant_redirect">Yes <input '.$redirect_shop_options[0].'  type="radio" name="merchant_redirect" value="1" /></label><span style="line-height:25px;">( '.$this->l('redirect the buyer to the merchant website after payment has been made').' )</span></td></tr>
					<tr><td  style="height: 35px;">'.$this->l('Redirect after payment confirm ?').'</td><td><input  type="text" style="width:50px;" name="redirect_timer" value="'.$this->redirect_timer.'"/> '.$this->l('(0 for instant redirect - time in seconds)').'</td></tr>
					<tr><td colspan="2" align="center"><input class="button" name="btnSubmit" value="'.$this->l('Update settings').'" type="submit" /></td></tr>
				</table><br/><br/>
				'.$this->error.' 
			</fieldset>
		</form>';
	}


	 public function getPayuKeys(){

		/* GETS THE SECRET KEY OF PAYU */
		$this->merchant_id = Configuration::get('payu_merchant_id'); 

		/* GETS THE SECRET KEY OF PAYU */
		$this->secret_key = Configuration::get('payu_secret_key'); 


		/* GETS THE PAYU STATE - IS TEST */
		$this->is_test = Configuration::get('payu_state_is_test'); 

		/* GETS THE PAYU STATE - IS IDN ACTIVE */
		$this->is_idn_active = Configuration::get('payu_state_is_idn_active'); 


		/* GETS THE PAYU REDIRECT TIMER */
		$this->redirect_timer = Configuration::get('payu_redirect_timer'); 

		/* GETS THE PAYU REDIRECT TO SHOP*/
		$this->redirect_to_shop = Configuration::get('payu_redirect_to_shop'); 
		
		


	}
}
