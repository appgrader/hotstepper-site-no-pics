<?php
global $smarty;
include(dirname(__FILE__).'/../../config/config.inc.php');
include(dirname(__FILE__).'/../../header.php');


	$customer = new Customer((int)$cart->id_customer);
	$total = $cart->getOrderTotal(true, Cart::BOTH);

	$smarty->assign(array(
		'total' => $total,
		'this_path_ssl' => Tools::getShopDomainSsl(true, true).__PS_BASE_URI__.'modules/payu/'
	));

	$smarty->assign('this_path', __PS_BASE_URI__.'modules/payu/');
	$template = '/validation.tpl';
	
	$smarty->display(dirname(__FILE__).$template);
	   

  include(dirname(__FILE__).'/../../footer.php');